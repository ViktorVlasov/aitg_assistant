#!/bin/bash

if [ ! -d "data_db" ]; then
    echo "Создается папка data_db..."
    mkdir data_db
else
    echo "Папка data_db уже существует."
fi

# Запуск docker-compose
echo "Запуск Docker Compose..."
docker compose up -d
