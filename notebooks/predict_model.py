from dotenv import load_dotenv
load_dotenv()

import pandas as pd
from argparse import ArgumentParser
import catboost
import mlflow
import mlflow.sklearn

print("Tracking URI:", mlflow.tracking.get_tracking_uri())
print("MLflow Version:", mlflow.__version__)
print("Catboost version:",catboost.__version__)

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--model-uri", dest="model_uri", 
                        help="model_uri", required=False, default=None)
    parser.add_argument("--model-name", dest="model_name", 
                        help="model_name", required=False, default=None)
    parser.add_argument("--model-stage", dest="model_stage", 
                        help="model_stage", required=False, default=None)
    
    args = parser.parse_args()
    print("Arguments:")
    for arg in vars(args):
        print(f"  {arg}: {getattr(args, arg)}")

    X = pd.DataFrame({
        'text': ["Продам гараж, писать в лс", """Тема: Kolmogorov Arnold Networks: 
                        новая архитектура нейронных сетей
                        Последние несколько месяцев активно обсуждается перспективная альтернатива 
                        для Многослойного перцептрона"""]
    })
    
    if args.model_uri:
        print("\n=== mlflow.sklearn.load_model")
        model = mlflow.sklearn.load_model(args.model_uri)
        print("model:", type(model))
        predictions = model.predict(X)
        print("predictions.type:", type(predictions))
        print("predictions.shape:", predictions.shape)
        print("predictions:", predictions)

        print("\n=== mlflow.pyfunc.load_model")
        model = mlflow.pyfunc.load_model(args.model_uri)
        print("model:", type(model))
        predictions = model.predict(X)
        print("predictions.type:", type(predictions))
        print("predictions.shape:", predictions.shape)
        print("predictions:", predictions)
    
    if args.model_name and args.model_stage:
        print("\n=== mlflow.pyfunc.load_model with model_name and model_stage")
        path = f"models:/{args.model_name}/{args.model_stage}"
        model = mlflow.pyfunc.load_model(path) 
        print("model:", type(model))
        predictions = model.predict(X)
        print("predictions.type:", type(predictions))
        print("predictions.shape:", predictions.shape)
        print("predictions:", predictions)

