from dotenv import load_dotenv
load_dotenv()
import os
import pandas as pd
from sklearn.model_selection import train_test_split
import mlflow
import mlflow.sklearn
import mlflow.onnx
import catboost
from catboost import CatBoostClassifier, Pool
from sklearn.metrics import classification_report, f1_score
import click
import random
import json

print("MLflow Version:", mlflow.version.VERSION)
print("MLflow Tracking URI:", mlflow.get_tracking_uri())
#print("XGBoost version:",xgb.__version__)
print("Catboost version:",catboost.__version__)
client = mlflow.tracking.MlflowClient()

random.seed(42)

def fit_model(train_pool, test_pool, **kwargs):
    model = CatBoostClassifier(
        task_type="CPU",
        # devices='0',
        # iterations=3000,
        od_type="Iter",
        od_wait=300,
        random_seed=13,
        # learning_rate=0.01,
        tokenizers=[
            {
                "tokenizer_id": "noSense",
                "separator_type": "BySense",
                "lowercasing": "True",
                "number_process_policy": "Replace",
                "token_types": ["Word", "Number", "SentenceBreak"],
                "sub_tokens_policy": "SeveralTokens",
            }
        ],
        dictionaries=[
            {
                "dictionary_id": "noWord",
                "max_dictionary_size": "50000",
            }
        ],
        feature_calcers=["BoW:top_tokens_count=10000", "NaiveBayes", "BM25"],
        boosting_type=(
            None#"Ordered" #if args.prod else None
        ),  # оч медленное. Отметить при подготовке продакшен-модели, пока забить
        **kwargs,
    )
    
    return model.fit(
        train_pool,
        eval_set=test_pool,
        verbose=100,
        plot=True,
        use_best_model=True,
    )

def build_data(data_path, lines):
    df = pd.read_json(data_path, lines=True).sample(lines, random_state=42, ignore_index=True)

    target_column = 'label'
    X = df.drop(target_column, axis=1)
    y = df[target_column]

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42, stratify=y, shuffle=True)

    return X_train, X_test, y_train, y_test

def train(data_path, iterations, learning_rate, lines, log_as_onnx, model_name):
    X_train, X_test, y_train, y_test = build_data(data_path, lines)
    with mlflow.start_run() as run:
        run_id = run.info.run_id
        experiment_id = run.info.experiment_id
        print("MLflow:")
        print("  run_id:", run_id)
        print("  experiment_id:", experiment_id)
        print("  experiment_name:", client.get_experiment(experiment_id).name)

        # MLflow params
        print("Parameters:")
        print("  learning_rate:", learning_rate)
        print("  iterations:", iterations)
        print("  lines:", lines)
        
        mlflow.log_param("learning_rate", learning_rate)
        mlflow.log_param("iterations", iterations)
        mlflow.log_param("lines", lines)
        
        # Create and fit model
        features = ['text']

        # prepare pool
        train_pool = Pool(
            X_train[features],
            label=y_train,
            text_features=["text"],
            feature_names=features
        )
        test_pool = Pool(
            X_test[features],
            label=y_test,
            text_features=["text"],
            feature_names=features
        )
        model = fit_model(train_pool,
                            test_pool,
                            eval_metric="TotalF1",
                            iterations=iterations,
                            learning_rate=learning_rate)
        print("model.type=", type(model))

        # Log catboost model
        mlflow.sklearn.log_model(model, "catboost-model", registered_model_name=model_name)

        # Log ONNX model
        if log_as_onnx:
            path = "catboost.onnx"
            model.save_model(path, format="onnx")
            with open(path, "rb") as f:
                onnx_model = f.read()
            mlflow.onnx.log_model(onnx_model, "onnx-model", 
                registered_model_name=None if not model_name else f"{model_name}_onnx")
            
        # Predict on test set
        preds_labels = model.predict(test_pool).tolist()
        true_labels = y_test.tolist()

        # Calculate and log metrics
        f1 = f1_score(true_labels, preds_labels, average='weighted')
        mlflow.log_metric("f1_score", f1)

        # Generate and log classification report
        report = classification_report(true_labels, preds_labels)
        print(report)
        mlflow.log_text(report, "classification_report.txt")


@click.command()
@click.option("--experiment-name", help="Experiment name", default=None, type=str)
@click.option("--data-path", help="Data path", default="../data/train_binary.json", type=str)
@click.option("--model-name", help="Registered model name", default=None, type=str)
@click.option("--iterations", help="Iterations", default=1000, type=int)
@click.option("--lines", help="Lines for training", default=1000, type=int)
@click.option("--learning-rate", help="Learning rate", default=0.01, type=float)
@click.option("--log-as-onnx", help="log_as_onnx", default=False, type=bool)

def main(experiment_name, data_path, model_name, iterations, lines, learning_rate, log_as_onnx):
    print("Options:")
    for k,v in locals().items():
        print(f"  {k}: {v}")
    model_name = None if not model_name or model_name == "None" else model_name
    if experiment_name:
        mlflow.set_experiment(experiment_name)
    train(data_path, iterations, learning_rate, lines, log_as_onnx, model_name)

if __name__ == "__main__":
    main()