[Презентация](https://docs.google.com/presentation/d/1GumLkOVDNFRX59WujDoVfxQvUrR3gdYAYYoOPcxgDTs/edit?usp=sharing)

[Видео-демо](https://drive.google.com/file/d/1-Vs254PhSJcC1HrSG7PUKzXMG1ieyRN-/view?usp=sharing)

Установка:

Минимальные требования:
    Python ~3.10

0. Распаковка данных
   ```bash
   unzip data.zip
   ```
1. Установка инфраструктуры:
   ```bash
   cd infrastructure/
   chmod +x infrastructure.sh
   ./infrastructure.sh
   cd ..
   ```
   После этого шага у вас должно быть запущено 4 контейнера и создана заполненная директория /infrastructure/data_db
2. Установка poetry если его нет в системе.
    ```bash
    pip3 install poetry
    ```
3. Установка необходимых зависимостей.
    ```bash
    poetry install
    ```
4. Подготовка .env файла.
   1. Выполнить
        ```bash
        cp .env.example .env
        ```
   2. Открыть .env файл и поменять переменные TG_BOT_TOKEN и API_KEY
   3. Для токенов для тг и OpenAI можно написать @messlav или @rhythm_00.
5. Векторизация и добавление данных в бд (без них RAG не работает). Занимает около 5 минут. Обязательно включить VPN
    ```bash
    poetry shell
    python src/rag/add_data.py
    ```

Следующие шаги выполнять из корня проекта:
Запуск
```bash
python3 src/run.py
```

Тестирование
```bash
python3 src/tests/test_dff.py
```