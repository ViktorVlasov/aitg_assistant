from dff.pipeline import Pipeline
from dff.script import Context

from src.chat.chat import chat_with_history
from src.chat.postprocessing import postprocessing
from src.chat.preprocessing import preprocessing
from src.rag.rag_qa import get_answer_with_history
from src.utils.chat_history import CustomRedisChatMessageHistory


def clear_history(ctx: Context, _: Pipeline) -> Context:
    request = ctx.last_request
    if not hasattr(request, 'update'):
        user_id = 7777  # test id
    else:
        user_id = request.update.from_user.id

    redis_chat_history = CustomRedisChatMessageHistory(user_id, url="redis://localhost:6379/0")
    redis_chat_history.clear
    return ctx


def generate_response_rag(ctx: Context, _: Pipeline) -> Context:
    request = ctx.last_request
    if not hasattr(request, 'update'):
        user_id = 7777  # test id
    else:
        user_id = request.update.from_user.id
    query = request.text

    answer = get_answer_with_history(query, user_id)

    ctx.misc['answer'] = answer["answer"]
    ctx.misc['sources'] = [document.metadata['source'] for document in answer["context"]]

    return ctx


def generate_response_chat(ctx: Context, _: Pipeline) -> Context:
    request = ctx.last_request
    if not hasattr(request, 'update'):
        user_id = 7777  # test id
    else:
        user_id = request.update.from_user.id
    query = request.text

    WARNING, cos_sim = preprocessing(user_input=query)
    if not WARNING:
        answer = chat_with_history(query, user_id)
        WARNING, cos_sim = postprocessing(query, WARNING)

    if WARNING:
        answer = "😊"

    ctx.misc['answer'] = answer
    return ctx
