from dff.pipeline import Pipeline
from dff.script import Context, Message


def rag_response(ctx: Context, _: Pipeline) -> Message:
    sources = ' '.join(ctx.misc['sources'])
    return Message(f"Ответ: {ctx.misc['answer']}\n\nИсточники: {' '.join(set(sources.split()))}")


def basic_response(ctx: Context, _: Pipeline) -> Message:
    return Message(f"{ctx.misc['answer']}")
