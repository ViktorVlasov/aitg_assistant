import dff.script.conditions as cnd
from dff.script import PRE_RESPONSE_PROCESSING, RESPONSE, TRANSITIONS, Message

from src.dialog_graph.processing import (
    clear_history,
    generate_response_chat,
    generate_response_rag,
)
from src.dialog_graph.response import basic_response, rag_response

GREETING = '''Привет! Я ваш AI помощник для решения NLP задач. Вот как я могу помочь.

1. Сначала вы должны выбрать режим работы (его можно менять в ходе общения). Введите одну из команд:
- /chat - режим обычного чата, где можно свободно общаться на любые темы.
- /qa - режим QA, где вы можете задать мне вопрос по NLP, и я найду на него ответ, используя данные из профильных NLP каналов.

2. На данный момент, в качестве контекста я использую 6 последних сообщений нашего диалога, поэтому эта команда может тебе пригодиться:
- /clear_history - очистить контекст.

Выбери режим работы, чтобы начать наше взаимодействие!'''

FALLBACK = "Пожалуйста, сначала выберите один из режимов работы. Введите /qa или /chat."

RAG_GREETING = '''Вы вошли в режим ответов на вопросы (QA). Если вы хотите перейти в режим чата, просто введите команду /chat.
Задайте любой вопрос, связанный с NLP, и я постараюсь найти на него максимально точный ответ, используя информацию из специализированных NLP каналов.
'''

CHAT_GREETING = '''Теперь вы находитесь в режиме обычного чата. Можем беседовать на любые темы!
Напишите что-нибудь, и я отвечу. Чтобы выйти из этого режима или воспользоваться другими функциями, просто введите соответствующую команду.
'''
CLEAR_MSG = "Ваша история чата была очищена."

common_transitions = {
    ("basic_flow", "basic_greeting_node"): cnd.exact_match(Message("/chat")),
    ("rag_flow", "rag_greeting_node"): cnd.exact_match(Message("/qa")),
    ("basic_flow", "clear_node"): cnd.exact_match(Message("/clear_history")),
}

script = {
    "greeting_flow": {
        "start_node": {
            RESPONSE: Message(),
            TRANSITIONS: {
                ("greeting_flow", "greeting_node"): cnd.exact_match(Message("/start")),
            },
        },
        "greeting_node": {
            RESPONSE: Message(GREETING),
            TRANSITIONS: {
                ("basic_flow", "basic_greeting_node"): cnd.exact_match(Message("/chat")),
                ("rag_flow", "rag_greeting_node"): cnd.exact_match(Message("/qa")),
                ("greeting_flow", "fallback_node"): cnd.true()
            }
        },
        "fallback_node": {
            RESPONSE: Message(FALLBACK),
            TRANSITIONS: {
                ("basic_flow", "basic_greeting_node"): cnd.exact_match(Message("/chat")),
                ("rag_flow", "rag_greeting_node"): cnd.exact_match(Message("/qa")),
                ("greeting_flow", "fallback_node"): cnd.true(),
            },
        },
    },
    "rag_flow": {
        "rag_greeting_node": {
            RESPONSE: Message(RAG_GREETING),
            TRANSITIONS: {
                **common_transitions,
                ("rag_flow", "response_node"): cnd.true(),
            },
        },
        "response_node": {
            PRE_RESPONSE_PROCESSING: {"1": generate_response_rag},
            RESPONSE: rag_response,
            TRANSITIONS: {
                **common_transitions,
                ("rag_flow", "response_node"): cnd.true(),
            },
        },
        "clear_node": {
            PRE_RESPONSE_PROCESSING: {"1": clear_history},
            RESPONSE: Message(CLEAR_MSG),
            TRANSITIONS: {
                **common_transitions,
                ("rag_flow", "response_node"): cnd.true(),
            },
        }
    },
    "basic_flow": {
        "basic_greeting_node": {
            RESPONSE: Message(CHAT_GREETING),
            TRANSITIONS: {
                **common_transitions,
                ("basic_flow", "response_node"): cnd.true(),
            },
        },
        "response_node": {
            PRE_RESPONSE_PROCESSING: {"1": generate_response_chat},
            RESPONSE: basic_response,
            TRANSITIONS: {
                **common_transitions,
                ("basic_flow", "response_node"): cnd.true(),
            },
        },
        "clear_node": {
            PRE_RESPONSE_PROCESSING: {"1": clear_history},
            RESPONSE: Message(CLEAR_MSG),
            TRANSITIONS: {
                **common_transitions,
                ("rag_flow", "response_node"): cnd.true(),
            },
        }
    },
}
