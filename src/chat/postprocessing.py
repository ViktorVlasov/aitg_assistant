import os

from src.chat.preprocessing import cosine_similarity, get_embedding
from src.prompts.basic_chat import basic_instructions

initial_prompt = basic_instructions
_initial_prompt_embedding = None

def initial_prompt_embedding(model):
    global _initial_prompt_embedding
    if _initial_prompt_embedding is None:
        _initial_prompt_embedding = get_embedding(initial_prompt, model)
    return _initial_prompt_embedding


def postprocessing(response, WARNING):
    """ return WARNING, cos_sim """
    cosine_threshold = float(os.getenv('COSINE_THRESHOLD', '0.5'))
    embedding_model = os.getenv('EMB_MODEL', 'text-embedding-3-small')
    flag = int(os.getenv('PROCESSING_FLAG', '0'))

    if not flag or WARNING:
        return WARNING, 0

    response_embedding = get_embedding(response, embedding_model)
    response_sim = cosine_similarity(response_embedding, initial_prompt_embedding(embedding_model))
    if response_sim > cosine_threshold:
        print("Response too similar to system prompt, sending a smile instead.")
        return 1, response_sim
    return 0, response_sim
