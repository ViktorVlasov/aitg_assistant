import os

from langchain_core.prompts import ChatPromptTemplate, MessagesPlaceholder
from langchain_core.runnables.history import RunnableWithMessageHistory
from langchain_openai import ChatOpenAI

from src.prompts.basic_chat import basic_instructions
from src.utils.chat_history import CustomRedisChatMessageHistory


def chat_with_history(query, user_id):
    chat = ChatOpenAI(model=os.getenv("BASE_MODEL", "gpt-3.5-turbo"),
                      api_key=os.getenv("API_KEY"))
    prompt_template = ChatPromptTemplate.from_messages([
        ("system", basic_instructions),
        MessagesPlaceholder(variable_name="chat_history"),
        ("human", "{input}"),
    ])

    chain = prompt_template | chat
    chain_with_message_history = RunnableWithMessageHistory(
        chain,
        lambda session_id: CustomRedisChatMessageHistory(
            session_id, url="redis://localhost:6379/0"
        ),
        input_messages_key="input",
        history_messages_key="chat_history",
    )

    response = chain_with_message_history.invoke(
        input={"input": query},
        config={"configurable": {"session_id": str(user_id)}},
    )

    return response.content
