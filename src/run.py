import os

from dff.messengers.telegram import PollingTelegramInterface
from dff.pipeline import Pipeline

from src.dialog_graph.script import script


def get_pipeline(use_cli_interface: bool = False) -> Pipeline:
    telegram_token = os.getenv("TG_BOT_TOKEN")

    if use_cli_interface:
        messenger_interface = None
    elif telegram_token:
        messenger_interface = PollingTelegramInterface(token=telegram_token)

    else:
        raise RuntimeError(
            "Telegram token (`TG_BOT_TOKEN`) is not set. `TG_BOT_TOKEN` can be set via `.env` file."
            "For more info see README.md."
        )

    pipeline = Pipeline.from_script(
        script,
        start_label=("greeting_flow", "start_node"),
        fallback_label=("greeting_flow", "fallback_node"),
        messenger_interface=messenger_interface,
    )

    return pipeline

if __name__ == "__main__":
    # load_dotenv(dotenv_path='.') Обнаружил, что load_dotenv как то странно работает с poetry
    # пока что без докера для основного приложения, поэтому быстрый, но рабочий фикс:
    with open('.env', 'r') as file:
        for line in file:
            if line.startswith('#') or not line.strip():
                continue
            key, value = line.strip().split('=', 1)
            os.environ[key] = value

    pipeline = get_pipeline()
    pipeline.run()
