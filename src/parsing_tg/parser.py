import json
import os
from datetime import datetime

from telethon.sync import TelegramClient
from telethon.tl.functions.messages import GetDialogsRequest, GetHistoryRequest
from telethon.tl.types import InputPeerEmpty
from tqdm import tqdm

api_id = 21898744
api_hash = "baf5e4ba4b3d4f77ee260786589b0b23"
phone = "+79097535941"

client = TelegramClient(phone, api_id, api_hash)

def connect_client():
    client.start()

def get_groups(client):
    chats = []
    last_date = None
    size_chats = 200
    result = client(GetDialogsRequest(
        offset_date=last_date,
        offset_id=0,
        offset_peer=InputPeerEmpty(),
        limit=size_chats,
        hash=0
    ))
    chats.extend(result.chats)

    return [chat for chat in chats]

def choose_group(groups):
    print("Выберите группу для парсинга сообщений и членов группы:")
    for i, group in enumerate(groups):
        print(f"{i}- {group.title}")
    g_index = input("Введите нужную цифру: ")
    return groups[int(g_index)]

def save_messages_batch_to_json(messages_batch, batch_index, channel_name):
    if not messages_batch:  # Проверка на пустоту списка
        return
    # Преобразование названия канала для использования в пути
    channel_dir = f"data/raw/{channel_name.lower().replace(' ', '_')}"
    os.makedirs(channel_dir, exist_ok=True)  # Создание директории, если не существует

    first_message_id = messages_batch[0]['id']
    last_message_id = messages_batch[-1]['id']
    file_name = (
        f"{channel_dir}/messages_batch_{batch_index}_"
        f"from_{first_message_id}_to_{last_message_id}.json"
    )
    with open(file_name, "w", encoding="utf-8") as json_file:
        json.dump(messages_batch, json_file, ensure_ascii=False, default=default_converter)

def default_converter(o):
    if isinstance(o, datetime):
        return o.isoformat()

def get_all_messages(client, target_group, total_count_limit=0, limit=5, batch_size=100):
    offset_id = 0
    all_messages = []
    batch_index = 0
    with tqdm(total=total_count_limit) as pbar:
        while True:
            history = client(GetHistoryRequest(
                peer=target_group,
                offset_id=offset_id,
                offset_date=None,
                add_offset=0,
                limit=limit,
                max_id=0,
                min_id=0,
                hash=0
            ))
            if not history.messages:
                break
            messages = history.messages
            all_messages.extend([message.to_dict() for message in messages])
            offset_id = messages[-1].id

            pbar.update(len(messages))
            # Проверка, достигнут ли предел батча или общий предел сообщений
            if (len(all_messages) >= batch_size
                or (total_count_limit != 0
                and len(all_messages) >= total_count_limit)):
                save_messages_batch_to_json(all_messages[:batch_size],
                                            batch_index,
                                            target_group.title)
                all_messages = all_messages[batch_size:]  # Удаляем сохраненные сообщения из списка
                batch_index += 1
            # Досрочное прерывание, если достигнут общий лимит сообщений
            if total_count_limit != 0 and batch_index * batch_size >= total_count_limit:
                break

        # Сохранение оставшихся сообщений, если они есть
        if all_messages:
            save_messages_batch_to_json(all_messages, batch_index, target_group.title)

def main():
    connect_client()
    groups = get_groups(client)
    target_group = choose_group(groups)

    total_limit = 0
    limit = 200
    get_all_messages(client, target_group, total_limit, limit)
    print('Парсинг сообщений группы успешно выполнен.')

if __name__ == '__main__':
    main()
