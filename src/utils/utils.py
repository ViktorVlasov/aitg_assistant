from langchain_openai import OpenAIEmbeddings


def get_embeddings_model(name_model: str, api_key: str):
    embeddings = OpenAIEmbeddings(model=name_model,
                                  api_key=api_key)
    return embeddings
