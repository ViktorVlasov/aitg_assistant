import os

from langchain_postgres.vectorstores import PGVector

from src.utils.utils import get_embeddings_model


def generate_conn_string():
    connection_string = PGVector.connection_string_from_db_params(
        driver=os.environ.get("PGVECTOR_DRIVER", "psycopg"),
        host=os.environ.get("PGVECTOR_HOST", "localhost"),
        port=int(os.environ.get("PGVECTOR_PORT", "5432")),
        database=os.environ.get("PGVECTOR_DATABASE", "postgres"),
        user=os.environ.get("PGVECTOR_USER", "postgres"),
        password=os.environ.get("PGVECTOR_PASSWORD", "password"),
    )
    return connection_string


def create_vectorstore(connection_string: str,
                        collection_name: str,
                        embeddings) -> None:
        vectorstore = PGVector(
            embeddings=embeddings,
            connection=connection_string,
            collection_name=collection_name,
            use_jsonb=True,
        )
        return vectorstore

def get_vectorestore() -> PGVector:
    conn = generate_conn_string()
    embeddings_model = get_embeddings_model(os.getenv('EMB_MODEL'),
                                            os.getenv('API_KEY'))
    vectore_store = create_vectorstore(conn, os.getenv('COLLECTION_NAME'), embeddings_model)
    return vectore_store
