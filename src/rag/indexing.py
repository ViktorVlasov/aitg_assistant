import tiktoken
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain_postgres.vectorstores import PGVector
from tqdm.auto import tqdm

tokenizer = tiktoken.get_encoding('cl100k_base')

def tiktoken_len(text):
    tokens = tokenizer.encode(
        text,
        disallowed_special=()
    )
    return len(tokens)

def insert_data_in_db(data, vectore_store: PGVector, batch_limit=100):
    text_splitter = RecursiveCharacterTextSplitter(
        chunk_size=400,
        chunk_overlap=20,
        length_function=tiktoken_len,
        separators=["\n\n", "\n", " ", ""]
    )

    texts = []
    metadatas = []
    for i, record in enumerate(tqdm(data)):
        metadata = {
            'message-id': str(record['id']),
            'source': record['url'],
        }

        record_texts = text_splitter.split_text(record['text'])
        record_metadatas = [{
            "chunk": j, "text": text, **metadata
        } for j, text in enumerate(record_texts)]

        texts.extend(record_texts)
        metadatas.extend(record_metadatas)

        if len(texts) >= batch_limit:
            vectore_store.add_texts(texts, metadatas)
            texts = []
            metadatas = []

    if len(texts) > 0:
        vectore_store.add_texts(texts, metadatas)
