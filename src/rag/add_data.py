import os

import pandas as pd

from src.rag.indexing import insert_data_in_db
from src.rag.vectorstore import get_vectorestore


def get_data(path_to_file: str, name_channel: str):
    base_url = 'https://t.me/'
    data = pd.read_json(path_to_file)
    data = data[['id', 'message']]
    data['url'] = data['id'].apply(lambda x: f'{base_url}{name_channel}/{x}')
    data.rename(columns={'message': 'text'}, inplace=True)
    return data.to_dict(orient='records')

if __name__ == "__main__":

    configs = [
        {
        'PATH_TO_FILE': 'data/processed/плюшевый_питон.json',
        'NAME_CHANNEL': 'plush_python'
    },
        {
        'PATH_TO_FILE': 'data/processed/сиолошная.json',
        'NAME_CHANNEL': 'seeallochnaya'
    },
        {
        'PATH_TO_FILE': 'data/processed/старший_авгур.json',
        'NAME_CHANNEL': 'senior_augur'
    },
        {
        'PATH_TO_FILE': 'data/processed/эйай_ньюз.json',
        'NAME_CHANNEL': 'ai_newz'
    },
        {
        'PATH_TO_FILE': 'data/processed/dealer.json',
        'NAME_CHANNEL': 'dealerAI'
    }]

    # load_dotenv(dotenv_path='.') Обнаружил, что load_dotenv как то странно работает с poetry
    # пока что без докера для основного приложения, поэтому быстрый, но рабочий фикс:
    with open('.env', 'r') as file:
        for line in file:
            if line.startswith('#') or not line.strip():
                continue
            key, value = line.strip().split('=', 1)
            os.environ[key] = value
    vectore_store = get_vectorestore()
    for config in configs:
        print('Обработка канала:', config['NAME_CHANNEL'])
        data = get_data(config['PATH_TO_FILE'], config['NAME_CHANNEL'])
        insert_data_in_db(data, vectore_store, batch_limit=30)
