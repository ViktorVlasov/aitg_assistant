import os

from dotenv import load_dotenv
from langchain.chains.combine_documents import create_stuff_documents_chain
from langchain.chains.history_aware_retriever import create_history_aware_retriever
from langchain.chains.retrieval import create_retrieval_chain
from langchain_core.runnables.history import RunnableWithMessageHistory
from langchain_openai import ChatOpenAI

from src.prompts.rag_with_history import contextualize_q_prompt, qa_prompt
from src.rag.vectorstore import get_vectorestore
from src.utils.chat_history import CustomRedisChatMessageHistory


def get_answer_with_history(query, user_id):
    model = ChatOpenAI(
        openai_api_key=os.getenv("API_KEY"),
        model_name=os.getenv("BASE_MODEL", "gpt-3.5-turbo"),
        temperature=float(os.getenv("TEMPERATURE", '0.0'))
    )

    vectorstore = get_vectorestore()
    retriever = vectorstore.as_retriever(
            search_type="similarity_score_threshold",
            search_kwargs={
                "score_threshold": float(os.getenv('SCORE_THRESHOLD', '0.5')),
                "k": int(os.getenv('TOP_K', '5'))
            },
        )

    history_aware_retriever = create_history_aware_retriever(
        model, retriever, contextualize_q_prompt
    )
    question_answer_chain = create_stuff_documents_chain(model, qa_prompt)
    rag_chain = create_retrieval_chain(history_aware_retriever, question_answer_chain)

    conversational_rag_chain = RunnableWithMessageHistory(
        rag_chain,
        lambda session_id: CustomRedisChatMessageHistory(
        session_id, url="redis://localhost:6379/0"
        ),
        input_messages_key="input",
        history_messages_key="chat_history",
        output_messages_key="answer",
    )

    answer = conversational_rag_chain.invoke(
        input={"input": query},
        config={"configurable": {"session_id": str(user_id)}})
    return answer


if __name__ == "__main__":
    load_dotenv()

    chat_history = []
    query = 'Какие существуют способы аугментации текстов на русском языке?'
    answer = get_answer_with_history(query, chat_history)
    chat_history.extend([query, answer["answer"]])

    print('Вопрос:', query)
    print('Ответ:', answer["answer"])
    print('Источники:', [document.metadata['source'] for document in answer["context"]])
    print()
