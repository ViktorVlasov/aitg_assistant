from typing import Any, Optional

from dff import Context
from dff.script import Message
from dff.utils.testing.common import (
    check_happy_path,
)

from src.dialog_graph.script import *
from src.run import *

happy_path = (
    # Старт общения и поддержка чата
    (
        Message("/start"),
        Message(GREETING),
    ),  # start_node -> greeting_node
    (
        Message("/chat"),
        Message(CHAT_GREETING),
    ),  # greeting_node -> basic_flow.basic_greeting_node
    (
        Message("Как дела? Ответь одним словом хорошо или плохо. Без знаков препинания, маленькими буквами"),
        Message("хорошо плохо"),
        # basic_greeting_node -> response_node
    ),
    # Переход в RAG
    (
        Message("/qa"),
        Message(RAG_GREETING),
    ),  # response_node -> rag_flow.rag_greeting_node
    (
        Message("Обьясни статью Retrieval Head Mechanistically Explains Long-Context Factuality"),
        Message("https://t.me/seeallochnaya/1329")
        # Message("""Статья "Retrieval Head Mechanistically Explains Long-Context Factuality" объясняет"""),
        # Message("Retrieval Head Mechanistically Explains Long-Context Factuality"),
        # Message("https://t.me/sioloshnaya/1329")
    ),  # response_node -> rag_flow.rag_greeting_node
    # Чистим историю
    (
        Message("/clear_history"),
        Message(CLEAR_MSG),
    ),  # response_node -> clear_node
    # Опять режим чата
    (
        Message("/chat"),
        Message(CHAT_GREETING),
    ),  # clear_node -> basic_flow.basic_greeting_node
    (
        Message("Как дела? Ответь одним словом хорошо или плохо. Без знаков препинания, маленькими буквами"),
        Message("хорошо плохо"),
    )
    # basic_greeting_node -> response_node
)


def test_comparer(candidate: Message, reference: Message, ctx: Context) -> Optional[Any]:
    """
    Our test comparer. If message not in predefined group - using default comparer
    If message in predefined group - using isin comparer

    :param candidate: The received (candidate) response.
    :param reference: The true (reference) response.
    :param _: Current Context (unused).
    :return: `None` if two responses are equal or candidate response otherwise.
    """
    predefined_messages_isin = ["Обьясни статью Retrieval Head Mechanistically Explains Long-Context Factuality"]
    if ctx.last_request.text in predefined_messages_isin:
        return None if reference.text in candidate.text else candidate

    predefined_messages_isin_split = ["Как дела? Ответь одним словом хорошо или плохо. Без знаков препинания, маленькими буквами"]
    if ctx.last_request.text in predefined_messages_isin_split:
        return None if candidate.text in reference.text.split() else candidate

    return None if candidate == reference else candidate


def test():
    with open('.env', 'r') as file:
        for line in file:
            if line.startswith('#') or not line.strip():
                continue
            key, value = line.strip().split('=', 1)
            os.environ[key] = value
    pipeline = get_pipeline()
    check_happy_path(
        pipeline,
        happy_path,
        response_comparer=test_comparer
    )


if __name__ == "__main__":
    test()
